<?php
session_start();

#Load the site config. 
require_once('inc/config.php');

if($site_config['login_enabled'] == 'true') {
	# Before we accept any user input...
	# Check the timestamp in /tmp/lastlogin -- if it's within the last 15 seconds, redirect
	# with a timeout message. 
	$filename = $site_config['temp']."/blog-lastlogin";
	
	if(file_exists($filename)) {
		$fh = fopen($filename,'r');
		$lastlogin = fread($fh,1024);
		fclose($fh);
	}
	
	# If the file doesn't exist, it's safe to move on.
	# before we go any further, write the timestamp out. 
	$current_date = new DateTime(null,new DateTimeZone('UTC'));
	$current_date_string = $current_date->format('Y-m-d H:i:sP');
	$fh = fopen($filename,'w');
	fwrite($fh,$current_date_string);
	fclose($fh);
	
	# $lastlogin should look something like a timestamp. If it doesn't, and it isn't empty,
	# then we should probably bounce to a redirect. 
	$last = new DateTime($lastlogin,new DateTimeZone('UTC'));
	$difference = $last->diff($current_date);
	if($difference->i > 0 or ($difference->i == 0 && $difference->s > 15)) {
		# Load the password database. Note that the password database should be out of the web path.
		$users = Spyc::YAMLLoad($site_config['users']);
	
		# Ok, now it's safe (ha) to accept user input.
		# If they match something in the protected yaml file, then allow them access.
		if($users[$_POST['userid']]) {
			# User exists.
			# Now check the password.
			$given_hash = crypt($_POST['pass'],'$2y$08$'.$users[$_POST['userid']['salt']]);
			if($given_hash == $users[$_POST['userid']['hash']]) {
				# User is valid. Set up the session and move on. 
			} else {
				# User is not valid. Reidrect to the login page.
				header('Location: /app/login.php?error=login');
			}
		}
	
	} else {
		# It's been too short a time since the last login attempt.
		header('Location: /app/login.php?error=timeout'); 
	}
}

#crypt('password','$2y$08$'.$site_config['salt']);

?>