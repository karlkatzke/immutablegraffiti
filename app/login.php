<?php
require_once('inc/config.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $site_config['title']; ?> - Application Login</title>
    <!-- Bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    
        <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>
  <body>


  
  	<script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.js"></script>

    <div class="container">
      <form class="form-signin" method="post" action="/app/login-do.php">
        <h2 class="form-signin-heading">Please sign in</h2>
                <?php 
        if(@$_GET['error'] == 'login') {
        	echo '<div class="alert alert-error"><i class="icon-remove"></i> Failed login. Please wait fifteen seconds before trying again.</div>';
        }
        if(@$_GET['error'] == 'timeout') {
        	echo '<div class="alert alert-error"><i class="icon-remove"></i> Failed login. Please wait fifteen seconds before trying again.</div>';
        }
       
         	?>
        <input name="userid" type="text" class="input-block-level" placeholder="User ID">
        <input name="pass" type="password" class="input-block-level" placeholder="Password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>
    </div> <!-- /container -->
  </body>
</html>
